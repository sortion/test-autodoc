// monprojet/include/cat.hpp

/**
 * @brief This is a cat
 */
#include <iostream>

class Cat {
public:
    Cat() {
        say("I'm a cat");
    }
    /**
     * @brief the cat is saying meow 
     */
    void meow()
    {
        say("meow");
    }

    /**
     * @brief the cat is saying something 
     * @param message the message to say
     */
    void say(const std::string& message)
    {
        std::cout << message << std::endl;
    }
};