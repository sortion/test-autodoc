# test-autodoc

Testing automatic documentation CI/CD pipeline with doxygen, sphinx and readthedocs.

Cet exemple fait l'objet d'un article sur bioinfo-fr: [Créer une documentation automatique avec Doxygen et Sphinx en CI/CD GitLab](https://bioinfo-fr.net/comment-creer-une-documentation-automatique-avec-doxygen-et-sphinx-en-ci-cd-gitlab)